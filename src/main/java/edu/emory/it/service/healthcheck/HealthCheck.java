/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, HealthCheck 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package edu.emory.it.service.healthcheck;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

//import org.apache.http.StatusLine;
////import org.apache.commons.httpclient.StatusLine;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;

public class HealthCheck {
    private static final int NTHREDS = 5;
    private String fileName = "healthcheck.properties";
    private Properties prop = new Properties();
    /**
     * bogus check
     */
    public Health doHealthCheck() throws Exception {
        Health h = new Health();
        h.setUrl("http://www.google.com");
        h.setStatus("ok");
        h.setResponseTimeMili(10);
        return h;
    }
    /**
     *
     * Single-threaded version
     */
    public Health[] doHealthChecks() throws Exception {
        String[] urls = getUrls();
        System.out.println("urls=" + prop.getProperty("urls"));
        Health[] healths = new Health[urls.length];
        for (int i = 0; i < urls.length; i++) {
            healths[i] = doHealthCheckFor(urls[i]);
            System.out.println("health" + i + "=" + healths[i]);
        }
        return healths;
    }
    private Health doHealthCheckFor(String url) {
        HttpClient httpclient = new HttpClient();
        GetMethod method = new GetMethod(url);

        Health health = new Health();
        health.setUrl(url);
        try {
            Long t1 = System.currentTimeMillis();
            int statusCode = httpclient.executeMethod(method);
            health.setStatus(String.valueOf(statusCode));
            health.setResponseTimeMili((int) (System.currentTimeMillis() - t1));
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return health;
    }
    /**
     *
     * Multi-threaded version
     */
    public Health[] performHealthChecks() throws Exception {
        String[] urls = getUrls();
        System.out.println("urls=" + prop.getProperty("urls"));
        Health[] healths = new Health[urls.length];

        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        List<Future<Health>> futures = new ArrayList<Future<Health>>();

        for (int i = 0; i < urls.length; i++) {
            final String url = urls[i];
            Callable<Health> worker = new Callable<Health>() {
                @Override
                public Health call() throws Exception {
                    return doHealthCheckFor(url);
                }
            };
            Future<Health> future = executor.submit(worker);
            futures.add(future);
        }

        for (int i = 0; i < urls.length; i++) {
            healths[i] = futures.get(i).get();
            System.out.println("health" + i + "=" + healths[i]);
        }
        return healths;
    }
    private String[] getUrls() throws Exception, IOException {
        InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
        if (input == null)
            throw new Exception("Unable to find file:" + fileName);
        prop.load(input);
        String[] urls = prop.getProperty("urls").trim().split(",");
        return urls;
    }

    // for newer httpClient version
    // private Health doHealthCheckFor(String url) {
    // CloseableHttpClient httpclient = HttpClients.createDefault();
    // HttpGet httpget = new HttpGet(url);
    // CloseableHttpResponse response = null;
    // Health health = new Health();
    // health.setUrl(url);
    // try {
    // Long t1 = System.currentTimeMillis();
    // response = httpclient.execute(httpget);
    // StatusLine statusLine = response.getStatusLine();
    // health.setStatus(statusLine.getReasonPhrase());
    // health.setResponseTimeMili((int) (System.currentTimeMillis() - t1));
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } finally {
    // try {
    // response.close();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }
    // return health;
    // }
}
