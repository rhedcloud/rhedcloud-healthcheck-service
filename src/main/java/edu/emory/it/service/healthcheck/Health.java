package edu.emory.it.service.healthcheck;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Health {
    protected static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public String toString() {
        return gson.toJson(this);
    }
    private String url;
    private String status;
    private Integer responseTimeMili;
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getResponseTimeMili() {
        return responseTimeMili;
    }
    public void setResponseTimeMili(Integer responseTimeMili) {
        this.responseTimeMili = responseTimeMili;
    }
}
